﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MoviesLibrary
{
    class AdditionalInfoUIElements
    {
        public Label VideoTitle { get; }
        public Label VideoRating { get; }
        public Label VideoYear { get; }
        public Label VideoGenre { get; }
        public Label VideoDuration { get; }
        public RichTextBox VideoDescription { get; }
        public LinkLabel URL { get; }
        public Button EditVideoButton { get; }

        public AdditionalInfoUIElements(Label videoTitle, Label videoRating, Label videoYear, Label videoGenre, Label videoDuration, RichTextBox videoDescription, LinkLabel urlLabel, Button editVideoButton)
        {
            VideoTitle = videoTitle;
            VideoRating = videoRating;
            VideoYear = videoYear;
            VideoGenre = videoGenre;
            VideoDuration = videoDuration;
            VideoDescription = videoDescription;
            URL = urlLabel;
            EditVideoButton = editVideoButton;
        }
        
        public void ShowAll()
        {
            VideoTitle.Visible = true;
            VideoRating.Visible = true;
            VideoYear.Visible = true;
            VideoGenre.Visible = true;
            VideoDuration.Visible = true;
            VideoDescription.Visible = true;
            URL.Visible = true;
            EditVideoButton.Visible = true;
        }

        public void HideAll()
        {
            VideoTitle.Visible = false;
            VideoRating.Visible = false;
            VideoYear.Visible = false;
            VideoGenre.Visible = false;
            VideoDuration.Visible = false;
            VideoDescription.Visible = false;
            URL.Visible = false;
            EditVideoButton.Visible = false;
        }
        
    }
}
