﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MoviesLibrary
{
    public partial class EditingVideo : Form
    {
        private MainForm _mainForm;
        private Video _videoToEdit;
        private Validator _validator = new Validator();

        public EditingVideo(MainForm mainForm, Video video)
        {
            InitializeComponent();
            _mainForm = mainForm;
            _videoToEdit = video;
        }

        private void EditingVideo_FormClosed(object sender, FormClosedEventArgs e)
        {
            _mainForm.Enabled = true;
        }

        private void FillForm()
        {
            textBox_Title.Text = _videoToEdit.Title;
            textBox_Rating.Text = _videoToEdit.Rating.ToString();
            textBox_Year.Text = _videoToEdit.Year.ToString();

            ComboOptionGenre comboOption;
            // fill the combobox and set the desired value
            foreach (Video.VideoGenre genre in Enum.GetValues(typeof(Video.VideoGenre)))
            {
                comboOption = new ComboOptionGenre(genre);

                comboBox_Genre.Items.Add(comboOption);

                if (genre == _videoToEdit.Genre)
                    comboBox_Genre.SelectedItem = comboOption;
                
            }
            
            if (_videoToEdit.Favorite)
                checkBox_Favorite.Checked = true;
            else
                checkBox_Favorite.Checked = false;

            if (_videoToEdit is Movie)
            {
                Movie movieToEdit = (Movie)_videoToEdit;
                textBox_Length.Text = movieToEdit.Length.ToString();
            }
            else if (_videoToEdit is TvShow)
            {
                TvShow tvShowToEdit = (TvShow)_videoToEdit;
                //comboBox_Seasons.Items.AddRange(tvShowToEdit.SeasonsAndEpisodes.ToArray());
                comboBox_Seasons.Items.AddRange(Helper.CopyListValuesToNewList(tvShowToEdit.SeasonsAndEpisodes).ToArray());
                comboBox_Seasons.SelectedIndex = 0;
            }

            textBox_URL.Text = _videoToEdit.URL;
            textBox_Description.Text = _videoToEdit.Description;

        }

        private void ActivateRightControls()
        {
            if (_videoToEdit is Movie)
            {
                comboBox_Seasons.Enabled = false;
                textBox_Episodes.Enabled = false;
                button_AddSeason.Enabled = false;
                button_RemoveSeason.Enabled = false;
            }
            else if (_videoToEdit is TvShow)
            {
                textBox_Length.Enabled = false;
            }
        }

        private void SaveChanges()
        {
            string url = "";

            if (textBox_URL.Text.Substring(0, 6) == "http://" || textBox_URL.Text.Substring(0, 7) == "https://")
                url = textBox_URL.Text;
            else
                url = String.Format("http://{0}", textBox_URL.Text);

            ComboOptionGenre comboOption = (ComboOptionGenre)comboBox_Genre.SelectedItem;
            _videoToEdit.Change(textBox_Title.Text, checkBox_Favorite.Checked, Int32.Parse(textBox_Year.Text), comboOption.Genre, Double.Parse(textBox_Rating.Text), textBox_Description.Text, url);
            
            if (_videoToEdit is Movie)
            {
                Movie movieToEdit = (Movie)_videoToEdit;
                movieToEdit.ChangeLength(Int32.Parse(textBox_Length.Text));
            }
            else if (_videoToEdit is TvShow)
            {
                TvShow tvShowToEdit = (TvShow)_videoToEdit;
                List<SeasonsEpisodes> seasonsEpisodesList = new List<SeasonsEpisodes>();
                
                foreach (object element in comboBox_Seasons.Items)
                {
                    if (element is SeasonsEpisodes)
                        seasonsEpisodesList.Add((SeasonsEpisodes)element);
                }

                tvShowToEdit.SaveNewCollectionOfSeasonsAndEpisodes(seasonsEpisodesList);

            }
        }

        private void EditingVideo_Load(object sender, EventArgs e)
        {
            // activate right text boxes depending on which type is video to edit (Movie or TvShow)
            ActivateRightControls();
            FillForm();
        }

        private void Button_Save_Click(object sender, EventArgs e)
        {
            if (_validator.ValidateToInt(textBox_Year, textBox_Length, textBox_Episodes) && _validator.ValidateToDouble(textBox_Rating))
            {
                SaveChanges();
                this.Close();
            }
            
        }

        private void Button_AddSeason_Click(object sender, EventArgs e)
        {
            int newSeasonNumber = comboBox_Seasons.Items.Count + 1;
            SeasonsEpisodes newSeasonsEpisodes = new SeasonsEpisodes(newSeasonNumber, 0);

            comboBox_Seasons.Items.Add(newSeasonsEpisodes);
            comboBox_Seasons.SelectedItem = newSeasonsEpisodes;
        }

        private void Button_RemoveSeason_Click(object sender, EventArgs e)
        {
            int numberOfSeasons = comboBox_Seasons.Items.Count;

            if (comboBox_Seasons.Items.Count > 1)
            {
                comboBox_Seasons.Items.RemoveAt(numberOfSeasons - 1);
                comboBox_Seasons.SelectedIndex = numberOfSeasons - 2;
            }
        }
        
        private void ComboBox_Seasons_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox_Seasons.SelectedItem is SeasonsEpisodes)
            {
                SeasonsEpisodes selectedItem = (SeasonsEpisodes)comboBox_Seasons.SelectedItem;
                textBox_Episodes.Text = selectedItem.NumberOfEpisodes.ToString();
            }
            
        }
        
        private void TextBox_Episodes_TextChanged(object sender, EventArgs e)
        {
            if (_validator.ValidateToInt(textBox_Episodes))
            {
                if (comboBox_Seasons.SelectedItem is SeasonsEpisodes)
                {
                    SeasonsEpisodes elementToChange = (SeasonsEpisodes)comboBox_Seasons.SelectedItem;

                    elementToChange.ChangeNumberOfEpisodes(Int32.Parse(textBox_Episodes.Text));
                }

            }
            
        }

    }
}
