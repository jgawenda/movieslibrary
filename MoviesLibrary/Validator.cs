﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace MoviesLibrary
{
    class Validator
    {
        private void TurnTextBoxRed(TextBox textBox)
        {
            textBox.BackColor = Color.Red;
        }

        private void TurnTextBoxWhite(TextBox textBox)
        {
            textBox.BackColor = Color.White;
        }

        public bool ValidateToInt(params TextBox[] textBoxes)
        {
            int output = 0;
            // flag to indicate if there were validate errors, true = no errors, false = errors
            bool flag = true;

            foreach (TextBox textBox in textBoxes)
            {
                if (!Int32.TryParse(textBox.Text, out output) && textBox.Enabled)
                {
                    flag = false;
                    TurnTextBoxRed(textBox);
                }
                else if (textBox.Enabled)
                    TurnTextBoxWhite(textBox);
            }

            return flag;
        }

        public bool ValidateToDouble(params TextBox[] textBoxes)
        {
            double output = 0;
            // flag to indicate if there were validate errors, true = no errors, false = errors
            bool flag = true;

            foreach (TextBox textBox in textBoxes)
            {
                if (!Double.TryParse(textBox.Text, out output) && textBox.Enabled)
                {
                    flag = false;
                    TurnTextBoxRed(textBox);
                }
                else if (textBox.Enabled)
                    TurnTextBoxWhite(textBox);
            }

            return flag;
        }

    }
}
