﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesLibrary
{
    class ComboOptionType
    {
        public Video.VideoType VideoType { get; }

        public ComboOptionType(Video.VideoType videoType)
        {
            VideoType = videoType;
        }

        public override string ToString()
        {
            return VideoType.ToString();
        }

    }
}
