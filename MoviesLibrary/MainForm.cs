﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MoviesLibrary.Operations;
using System.Diagnostics;

namespace MoviesLibrary
{
    public partial class MainForm : Form
    {
        private NewVideoAdder _newVideoAdder;
        private VideoMover _videoMover;
        private VideoRemover _videoRemover;
        private AdditionalInfoOperator _additionalInfoOperator;
        private EditingVideo _editingVideo;
        private VideosSaverLoader _videosSaverLoader;
        private VideosSorter _videosSorter;
        private ListBoxRefresher _listBoxRefresher;

        public ListBox ListBox_ToWatch
        {
            get
            {
                return listBox_ToWatch;
            }
        }

        public ListBox ListBox_Watched
        {
            get
            {
                return listBox_Watched;
            }
        }

        public Label Label_AlreadySaved
        {
            get
            {
                return label_AlreadySaved;
            }
        }

        public MainForm()
        {
            InitializeComponent();
            InitializeVideoTypeDropdown();
            InitializeSortingDropdown();
            dropdown_Type.SelectedIndex = 0;
            DisableVideoButtons();
            _newVideoAdder = new NewVideoAdder(this);
            _videoMover = new VideoMover(this);
            _videoRemover = new VideoRemover(this);
            _additionalInfoOperator = new AdditionalInfoOperator(new AdditionalInfoUIElements(label_VideoTitle, label_VideoRating, label_VideoYear, label_VideoGenre, label_VideoDuration, textBox_VideoDescription, linkLabel_WWW, button_EditVideo));
            _videosSaverLoader = new VideosSaverLoader("videosToWatch", "videosWatched");
            _videosSorter = new VideosSorter(listBox_ToWatch, listBox_Watched);
            _listBoxRefresher = new ListBoxRefresher(listBox_ToWatch, listBox_Watched);

            LoadVideosToListBoxes();
        }

        private void LoadVideosToListBoxes()
        {
            listBox_ToWatch.Items.AddRange(_videosSaverLoader.DeserializeToWatch());
            listBox_Watched.Items.AddRange(_videosSaverLoader.DeserializeWatched());
        }

        private void SaveVideos()
        {
            _videosSaverLoader.SaveToWatch(listBox_ToWatch);
            _videosSaverLoader.SaveWatched(listBox_Watched);
        }

        private void InitializeVideoTypeDropdown()
        {
            dropdown_Type.Items.Add(new ComboOptionType(Video.VideoType.Movie));
            dropdown_Type.Items.Add(new ComboOptionType(Video.VideoType.TvShow));
        }

        private void InitializeSortingDropdown()
        {
            foreach (var option in Enum.GetValues(typeof(VideosSorter.SortType)))
            {
                comboBox_SortBy.Items.Add(option);
            }
        }

        private void Button_Add_Click(object sender, EventArgs e)
        {
            if (textBox_AddNew.Text != "" && dropdown_Type.SelectedItem != null)
            {
                if (_newVideoAdder.AddNewVideo(textBox_AddNew.Text, (ComboOptionType)dropdown_Type.SelectedItem))
                    textBox_AddNew.Clear();
            }
        }

        private void Button_MoveRight_Click(object sender, EventArgs e)
        {
            _videoMover.MoveSelectedRight();
        }

        private void Button_MoveLeft_Click(object sender, EventArgs e)
        {
            _videoMover.MoveSelectedLeft();
        }
        
        private void ListBox_ToWatch_MouseDown(object sender, MouseEventArgs e)
        {
            listBox_Watched.ClearSelected();
        }

        private void ListBox_Watched_MouseDown(object sender, MouseEventArgs e)
        {
            listBox_ToWatch.ClearSelected();
        }

        private void Button_Remove_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to delete this video?", "Removing video", MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
                _videoRemover.RemoveSelected();
        }

        private void ListBox_ToWatch_SelectedValueChanged(object sender, EventArgs e)
        {
            if (listBox_ToWatch.SelectedItem != null)
            {
                button_MoveLeft.Enabled = false;
                button_MoveRight.Enabled = true;
                button_Remove.Enabled = true;
                _additionalInfoOperator.DisplayVideoInfo((Video)listBox_ToWatch.SelectedItem);
                _additionalInfoOperator.DisplayAllElements();
            }
            else
            {
                _additionalInfoOperator.HideAllElements();
                DisableVideoButtons();
            }
        }

        private void ListBox_Watched_SelectedValueChanged(object sender, EventArgs e)
        {
            if (listBox_Watched.SelectedItem != null)
            {
                button_MoveRight.Enabled = false;
                button_MoveLeft.Enabled = true;
                button_Remove.Enabled = true;
                _additionalInfoOperator.DisplayVideoInfo((Video)listBox_Watched.SelectedItem);
                _additionalInfoOperator.DisplayAllElements();
            }
            else
            {
                _additionalInfoOperator.HideAllElements();
                DisableVideoButtons();
            }
        }

        public void DisableVideoButtons()
        {
            button_MoveLeft.Enabled = false;
            button_MoveRight.Enabled = false;
            button_Remove.Enabled = false;
        }

        private void Button_EditVideo_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            _editingVideo = new EditingVideo(this, _additionalInfoOperator.VideoToShow);
            _editingVideo.Show();
        }

        private void LinkLabel_WWW_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(_additionalInfoOperator.VideoToShow.URL);
        }

        private void MainForm_EnabledChanged(object sender, EventArgs e)
        {
            if (Enabled)
            {
                _listBoxRefresher.Refresh();

                _additionalInfoOperator.RefreshElements();
            }
        }
        
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveVideos();
        }

        private void ComboBox_SortBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox_SortBy.SelectedItem is VideosSorter.SortType)
            {
                VideosSorter.SortType sortType = (VideosSorter.SortType)comboBox_SortBy.SelectedItem;

                _videosSorter.Sort(sortType);
                //_listBoxRefresher.Refresh();
            }

        }

    }
}
