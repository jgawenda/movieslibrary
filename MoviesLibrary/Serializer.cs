﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace MoviesLibrary
{
    class Serializer
    {
        private string _serializationPath;

        public Serializer(string serializationPath)
        {
            _serializationPath = serializationPath;
        }

        public void Serialize(List<Video> arrayOfVideos)
        {
            using (Stream stream = File.Open(_serializationPath, FileMode.Create))
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(stream, arrayOfVideos);
                stream.Close();
            }

        }

        public List<Video> Deserialize()
        {
            if (File.Exists(_serializationPath))
            {
                using (Stream stream = File.Open(_serializationPath, FileMode.Open))
                {
                    BinaryFormatter binaryFormatter = new BinaryFormatter();
                    return (List<Video>)binaryFormatter.Deserialize(stream);
                }
            }

            return new List<Video>();
        }

    }
}
