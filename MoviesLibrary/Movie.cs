﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesLibrary
{
    [Serializable]
    class Movie : Video
    {
        public int Length { get; private set; }

        public Movie(string title, VideoType videoType) : base(title, videoType)
        {
            Length = 0;
        }

        public void ChangeLength(int newLength)
        {
            Length = newLength;
        }

    }
}
