﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MoviesLibrary.Operations
{
    class ListBoxRefresher
    {
        private ListBox _listBoxToWatch;
        private ListBox _listBoxWatched;

        public ListBoxRefresher(ListBox listBoxToWatch, ListBox listBoxWatched)
        {
            _listBoxToWatch = listBoxToWatch;
            _listBoxWatched = listBoxWatched;
        }
        
        public void Refresh()
        {
            // refreshing ToWatch list
            object[] collection = new object[_listBoxToWatch.Items.Count];
            _listBoxToWatch.Items.CopyTo(collection, 0);
            _listBoxToWatch.Items.Clear();
            _listBoxToWatch.Items.AddRange(collection);

            // refreshing Watched list
            collection = new object[_listBoxWatched.Items.Count];
            _listBoxWatched.Items.CopyTo(collection, 0);
            _listBoxWatched.Items.Clear();
            _listBoxWatched.Items.AddRange(collection);
        }

    }
}
