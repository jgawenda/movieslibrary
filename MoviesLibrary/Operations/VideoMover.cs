﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesLibrary.Operations
{
    class VideoMover
    {
        private MainForm _mainForm;

        public VideoMover(MainForm mainForm)
        {
            _mainForm = mainForm;
        }

        public void MoveSelectedRight()
        {
            Video videoToMove = (Video)_mainForm.ListBox_ToWatch.SelectedItem;

            // check if there is any selected video
            if (videoToMove != null)
            {
                // remove from ToWatch Listbox and add to Watched ListBox
                _mainForm.ListBox_ToWatch.Items.Remove(videoToMove);
                _mainForm.ListBox_Watched.Items.Add(videoToMove);
            }
            
        }

        public void MoveSelectedLeft()
        {
            Video videoToMove = (Video)_mainForm.ListBox_Watched.SelectedItem;

            if (videoToMove != null)
            {
                _mainForm.ListBox_Watched.Items.Remove(videoToMove);
                _mainForm.ListBox_ToWatch.Items.Add(videoToMove);
            }
        }

    }
}
