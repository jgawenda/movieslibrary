﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MoviesLibrary.Operations
{
    class NewVideoAdder
    {
        private MainForm _mainForm;

        public NewVideoAdder(MainForm mainForm)
        {
            _mainForm = mainForm;
        }

        private bool IsThereSameVideoAlready(Video videoToCheck)
        {
            foreach (object item in _mainForm.ListBox_ToWatch.Items)
            {
                if (item is Video)
                {
                    Video vidFromList = (Video)item;
                    
                    if (vidFromList.Title.ToLower() == videoToCheck.Title.ToLower() && vidFromList.Type == videoToCheck.Type)
                        return true;
                }
            }

            foreach (object item in _mainForm.ListBox_Watched.Items)
            {
                if (item is Video)
                {
                    Video vidFromList = (Video)item;

                    if (vidFromList.Title.ToLower() == videoToCheck.Title.ToLower() && vidFromList.Type == videoToCheck.Type)
                        return true;
                }
            }

            return false;
        }

        public bool AddNewVideo(string title, ComboOptionType comboOptionType)
        {
            //string item = String.Format("{0} ({1})", title, type);
            //Video video = new Video(title, type);

            Video video;
            
            switch (comboOptionType.VideoType)
            {
                case Video.VideoType.Movie:
                    video = new Movie(title, comboOptionType.VideoType);
                    break;
                case Video.VideoType.TvShow:
                    video = new TvShow(title, comboOptionType.VideoType);
                    break;
                default:
                    video = new Video(title, comboOptionType.VideoType);
                    break;
            }

            if (!IsThereSameVideoAlready(video))
            {
                _mainForm.Label_AlreadySaved.Visible = false;
                _mainForm.ListBox_ToWatch.Items.Add(video);
                return true;
            }
            else
            {
                _mainForm.Label_AlreadySaved.Visible = true;
                return false;
            }
            
        }

    }
}
