﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MoviesLibrary.Operations
{
    class VideosSaverLoader
    {
        private Serializer _serializerToWatch;
        private Serializer _serializerWatched;

        public VideosSaverLoader(string toWatchFilePath, string watchedFilePath)
        {
            _serializerToWatch = new Serializer(toWatchFilePath);
            _serializerWatched = new Serializer(watchedFilePath);
        }
        
        private void Save(ListBox listBox, Serializer serializer)
        {
            List<Video> videos = new List<Video>();
            
            foreach (var element in listBox.Items)
            {
                if (element is Video)
                    videos.Add((Video)element);
            }

            serializer.Serialize(videos);

        }

        private List<Video> Deserialize(Serializer serializer)
        {
            return serializer.Deserialize();
        }

        public void SaveToWatch(ListBox listBox)
        {
            Save(listBox, _serializerToWatch);

        }

        public void SaveWatched(ListBox listBox)
        {
            Save(listBox, _serializerWatched);

        }

        public Video[] DeserializeToWatch()
        {
            List<Video> videos = Deserialize(_serializerToWatch);

            return videos.ToArray();
        }

        public Video[] DeserializeWatched()
        {
            List<Video> videos = Deserialize(_serializerWatched);

            return videos.ToArray();
        }

    }
}
