﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MoviesLibrary.Operations
{
    class VideosSorter
    {
        public enum SortType
        {
            Default,
            Title,
            Genre,
            Favorite,
            Year
        }
        
        private ListBox _listBoxToWatch;
        private ListBox _listBoxWatched;

        public VideosSorter(ListBox listBoxToWatch, ListBox listBoxWatched)
        {
            _listBoxToWatch = listBoxToWatch;
            _listBoxWatched = listBoxWatched;
        }

        private List<Video> GetVideosList(ListBox listBox)
        {
            List<Video> listOfVideos = new List<Video>();
            
            foreach (object element in listBox.Items)
            {
                if (element is Video)
                {
                    Video videoToSave = (Video)element;
                    listOfVideos.Add(videoToSave);
                }
            }

            return listOfVideos;

        }

        private void SaveChangedLists(List<Video> newVideosToWatch, List<Video> newVideosWatched)
        {
            _listBoxToWatch.Items.Clear();
            _listBoxToWatch.Items.AddRange(newVideosToWatch.ToArray());

            _listBoxWatched.Items.Clear();
            _listBoxWatched.Items.AddRange(newVideosWatched.ToArray());
        }

        private void SortByDateCreated(List<Video> videosToSortToWatch, List<Video> videosToSortWatched)
        {
            videosToSortToWatch.Sort((x, y) => x.DateCreated.CompareTo(y.DateCreated));
            videosToSortWatched.Sort((x, y) => x.DateCreated.CompareTo(y.DateCreated));

            SaveChangedLists(videosToSortToWatch, videosToSortWatched);
        }

        private void SortByTitle(List<Video> videosToSortToWatch, List<Video> videosToSortWatched)
        {
            videosToSortToWatch.Sort((x, y) => x.Title.CompareTo(y.Title));
            videosToSortWatched.Sort((x, y) => x.Title.CompareTo(y.Title));

            SaveChangedLists(videosToSortToWatch, videosToSortWatched);
        }

        private void SortByGenre(List<Video> videosToSortToWatch, List<Video> videosToSortWatched)
        {
            videosToSortToWatch.Sort((x, y) => x.Genre.CompareTo(y.Genre));
            videosToSortWatched.Sort((x, y) => x.Genre.CompareTo(y.Genre));

            SaveChangedLists(videosToSortToWatch, videosToSortWatched);
        }

        private void SortByFavorite(List<Video> videosToSortToWatch, List<Video> videosToSortWatched)
        {
            videosToSortToWatch.Sort((x, y) => y.Favorite.CompareTo(x.Favorite));
            videosToSortWatched.Sort((x, y) => y.Favorite.CompareTo(x.Favorite));

            SaveChangedLists(videosToSortToWatch, videosToSortWatched);
        }

        private void SortByYear(List<Video> videosToSortToWatch, List<Video> videosToSortWatched)
        {
            videosToSortToWatch.Sort((x, y) => y.Year.CompareTo(x.Year));
            videosToSortWatched.Sort((x, y) => y.Year.CompareTo(x.Year));

            SaveChangedLists(videosToSortToWatch, videosToSortWatched);
        }
        
        public void Sort(SortType sortType)
        {
            List<Video> listToSortToWatch = GetVideosList(_listBoxToWatch);
            List<Video> listToSortWatched = GetVideosList(_listBoxWatched);

            switch (sortType)
            {
                case SortType.Default:
                    SortByDateCreated(listToSortToWatch, listToSortWatched);
                    break;
                case SortType.Title:
                    SortByTitle(listToSortToWatch, listToSortWatched);
                    break;
                case SortType.Genre:
                    SortByGenre(listToSortToWatch, listToSortWatched);
                    break;
                case SortType.Favorite:
                    SortByFavorite(listToSortToWatch, listToSortWatched);
                    break;
                case SortType.Year:
                    SortByYear(listToSortToWatch, listToSortWatched);
                    break;
            }

        }

    }
}
