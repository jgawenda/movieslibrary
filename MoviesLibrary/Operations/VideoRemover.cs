﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MoviesLibrary.Operations
{
    class VideoRemover
    {
        private MainForm _mainForm;

        public VideoRemover(MainForm mainForm)
        {
            _mainForm = mainForm;
        }
        
        public void RemoveSelected()
        {
            if (_mainForm.ListBox_ToWatch.SelectedItem != null)
                _mainForm.ListBox_ToWatch.Items.Remove(_mainForm.ListBox_ToWatch.SelectedItem);
            else if (_mainForm.ListBox_Watched.SelectedItem != null)
                _mainForm.ListBox_Watched.Items.Remove(_mainForm.ListBox_Watched.SelectedItem);

        }

    }
}
