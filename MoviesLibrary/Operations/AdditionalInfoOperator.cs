﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MoviesLibrary.Operations
{
    class AdditionalInfoOperator
    {
        private AdditionalInfoUIElements _uiElements;
        public Video VideoToShow { get; private set; }

        public AdditionalInfoOperator(AdditionalInfoUIElements uiElements)
        {
            _uiElements = uiElements;
        }

        public void HideAllElements()
        {
            _uiElements.HideAll();
        }

        public void DisplayAllElements()
        {
            _uiElements.ShowAll();
        }
        
        public void DisplayVideoInfo(Video video)
        {
            VideoToShow = video;

            string tempTitle = video.Title;
            if (tempTitle.Length > 20)
                tempTitle = String.Format("{0} ...", tempTitle.Substring(0, 20));

            _uiElements.VideoTitle.Text = tempTitle;
            _uiElements.VideoRating.Text = video.Rating.ToString();
            _uiElements.VideoYear.Text = video.Year.ToString();
            _uiElements.VideoGenre.Text = video.Genre.ToString();
            _uiElements.VideoDescription.Text = video.Description;

            if (video is Movie)
            {
                Movie movie = (Movie)video;
                string length = String.Format("{0} min", movie.Length);
                _uiElements.VideoDuration.Text = length;
            }
            else if (video is TvShow)
            {
                TvShow tvShow = (TvShow)video;
                string length = String.Format("{0} season(s)", tvShow.SeasonsAndEpisodes.Count);
                _uiElements.VideoDuration.Text = length;
            }

        }

        public void RefreshElements()
        {
            DisplayVideoInfo(VideoToShow);
        }
        
    }
}
