﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesLibrary
{
    static class Helper
    {
        public static List<SeasonsEpisodes> CopyListValuesToNewList(List<SeasonsEpisodes> seasonsEpisodesList)
        {
            List<SeasonsEpisodes> newListSeasonsEpisodes = new List<SeasonsEpisodes>();

            foreach (SeasonsEpisodes seasonsEpisodes in seasonsEpisodesList)
            {
                newListSeasonsEpisodes.Add(new SeasonsEpisodes(seasonsEpisodes.SeasonNumber, seasonsEpisodes.NumberOfEpisodes));
            }

            return newListSeasonsEpisodes;
        }

    }
}
