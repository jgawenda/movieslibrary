﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesLibrary
{
    [Serializable]
    class TvShow : Video
    {
        public List<SeasonsEpisodes> SeasonsAndEpisodes { get; private set; }

        public TvShow(string title, VideoType videoType) : base(title, videoType)
        {
            SeasonsAndEpisodes = new List<SeasonsEpisodes>();
            AddNextSeason(0);
        }
        
        private bool IsThereSameSeasonAlready(int seasonNumber)
        {
            foreach (SeasonsEpisodes season in SeasonsAndEpisodes)
            {
                if (season.SeasonNumber == seasonNumber)
                    return true;
            }

            return false;
        }

        public void AddNextSeason(int numberOfEpisodes)
        {
            int newSeasonNumber = SeasonsAndEpisodes.Count + 1;

            while (IsThereSameSeasonAlready(newSeasonNumber))
                newSeasonNumber++;

            SeasonsAndEpisodes.Add(new SeasonsEpisodes(newSeasonNumber, numberOfEpisodes));
        }
        
        public void SaveNewCollectionOfSeasonsAndEpisodes(List<SeasonsEpisodes> seasonsEpisodesList)
        {
            SeasonsAndEpisodes = seasonsEpisodesList;
        }

    }
}
