﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesLibrary
{
    [Serializable]
    public class Video
    {
        public enum VideoGenre
        {
            None,
            Action,
            Adventure,
            Animation,
            Biography,
            Comedy,
            Crime,
            Documentary,
            Drama,
            Family,
            Fantasy,
            History,
            Horror,
            Music,
            Musical,
            Mystery,
            Romance,
            SciFi,
            Short,
            Sport,
            Superhero,
            Thriller,
            War,
            Western
        }

        public enum VideoType
        {
            Movie,
            TvShow
        }

        public string Title { get; private set; }
        public VideoType Type { get; private set; }
        public bool Favorite { get; private set; }
        public int Year { get; private set; }
        public VideoGenre Genre { get; private set; }
        public double Rating { get; private set; }
        public string Description { get; private set; }
        public string URL { get; private set; }
        public DateTime DateCreated { get; }

        public Video(string title, VideoType videoType)
        {
            Title = title;
            Type = videoType;
            Favorite = false;
            Year = 0;
            Rating = 0;
            Genre = VideoGenre.None;
            Description = "Click edit to change the description...";
            URL = "google.com";
            DateCreated = DateTime.Now;
        }

        public void Change(string title, bool favorite, int year, VideoGenre genre, double rating, string description, string url)
        {
            Title = title;
            Favorite = favorite;
            Year = year;
            Genre = genre;
            Rating = rating;
            Description = description;
            URL = url;
        }
        
        public override string ToString()
        {
            string title = Title;
            if (title.Length > 20)
            {
                title = String.Format("{0}{1}", title.Substring(0, 20), "...");
            }

            return String.Format("{0}({1}) {2}", (Favorite) ? "* " : "", Type, title);
        }

    }
}
