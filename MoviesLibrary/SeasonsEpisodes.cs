﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesLibrary
{
    [Serializable]
    class SeasonsEpisodes
    {
        public int SeasonNumber { get; private set; }
        public int NumberOfEpisodes { get; private set; }

        public SeasonsEpisodes(int seasonNumber, int numberOfEpisodes)
        {
            SeasonNumber = seasonNumber;
            NumberOfEpisodes = numberOfEpisodes;
        }

        public void ChangeNumberOfEpisodes(int numberOfEpisodes)
        {
            NumberOfEpisodes = numberOfEpisodes;
        }

        public override string ToString()
        {
            return SeasonNumber.ToString();
        }

    }
}
