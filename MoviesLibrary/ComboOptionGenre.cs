﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesLibrary
{
    class ComboOptionGenre
    {
        public Video.VideoGenre Genre { get; }

        public ComboOptionGenre(Video.VideoGenre genre)
        {
            Genre = genre;
        }

        public override string ToString()
        {
            return Genre.ToString();
        }

    }
}
