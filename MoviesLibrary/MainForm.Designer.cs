﻿namespace MoviesLibrary
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.label_Header = new System.Windows.Forms.Label();
            this.listBox_ToWatch = new System.Windows.Forms.ListBox();
            this.textBox_AddNew = new System.Windows.Forms.TextBox();
            this.label_AddNew = new System.Windows.Forms.Label();
            this.label_ToWatch = new System.Windows.Forms.Label();
            this.label_Watched = new System.Windows.Forms.Label();
            this.listBox_Watched = new System.Windows.Forms.ListBox();
            this.dropdown_Type = new System.Windows.Forms.ComboBox();
            this.label_Title = new System.Windows.Forms.Label();
            this.label_Type = new System.Windows.Forms.Label();
            this.button_Add = new System.Windows.Forms.Button();
            this.button_MoveLeft = new System.Windows.Forms.Button();
            this.button_MoveRight = new System.Windows.Forms.Button();
            this.label_VideoTitle = new System.Windows.Forms.Label();
            this.label_VideoGenre = new System.Windows.Forms.Label();
            this.label_VideoDuration = new System.Windows.Forms.Label();
            this.label_VideoRating = new System.Windows.Forms.Label();
            this.textBox_VideoDescription = new System.Windows.Forms.RichTextBox();
            this.button_Remove = new System.Windows.Forms.Button();
            this.label_AlreadySaved = new System.Windows.Forms.Label();
            this.label_VideoYear = new System.Windows.Forms.Label();
            this.button_EditVideo = new System.Windows.Forms.Button();
            this.linkLabel_WWW = new System.Windows.Forms.LinkLabel();
            this.label_AutosaveInfo = new System.Windows.Forms.Label();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.comboBox_SortBy = new System.Windows.Forms.ComboBox();
            this.label_SortBy = new System.Windows.Forms.Label();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // label_Header
            // 
            this.label_Header.AutoSize = true;
            this.label_Header.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_Header.Location = new System.Drawing.Point(362, 13);
            this.label_Header.Name = "label_Header";
            this.label_Header.Size = new System.Drawing.Size(155, 29);
            this.label_Header.TabIndex = 0;
            this.label_Header.Text = "Video Library";
            // 
            // listBox_ToWatch
            // 
            this.listBox_ToWatch.FormattingEnabled = true;
            this.listBox_ToWatch.Location = new System.Drawing.Point(34, 209);
            this.listBox_ToWatch.Name = "listBox_ToWatch";
            this.listBox_ToWatch.Size = new System.Drawing.Size(195, 212);
            this.listBox_ToWatch.TabIndex = 1;
            this.listBox_ToWatch.SelectedValueChanged += new System.EventHandler(this.ListBox_ToWatch_SelectedValueChanged);
            this.listBox_ToWatch.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ListBox_ToWatch_MouseDown);
            // 
            // textBox_AddNew
            // 
            this.textBox_AddNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_AddNew.Location = new System.Drawing.Point(33, 100);
            this.textBox_AddNew.MaxLength = 50;
            this.textBox_AddNew.Name = "textBox_AddNew";
            this.textBox_AddNew.Size = new System.Drawing.Size(266, 29);
            this.textBox_AddNew.TabIndex = 2;
            // 
            // label_AddNew
            // 
            this.label_AddNew.AutoSize = true;
            this.label_AddNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_AddNew.Location = new System.Drawing.Point(30, 55);
            this.label_AddNew.Name = "label_AddNew";
            this.label_AddNew.Size = new System.Drawing.Size(86, 24);
            this.label_AddNew.TabIndex = 3;
            this.label_AddNew.Text = "Add new";
            // 
            // label_ToWatch
            // 
            this.label_ToWatch.AutoSize = true;
            this.label_ToWatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_ToWatch.Location = new System.Drawing.Point(30, 180);
            this.label_ToWatch.Name = "label_ToWatch";
            this.label_ToWatch.Size = new System.Drawing.Size(87, 24);
            this.label_ToWatch.TabIndex = 4;
            this.label_ToWatch.Text = "To watch";
            // 
            // label_Watched
            // 
            this.label_Watched.AutoSize = true;
            this.label_Watched.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_Watched.Location = new System.Drawing.Point(312, 179);
            this.label_Watched.Name = "label_Watched";
            this.label_Watched.Size = new System.Drawing.Size(150, 24);
            this.label_Watched.TabIndex = 5;
            this.label_Watched.Text = "Already watched";
            // 
            // listBox_Watched
            // 
            this.listBox_Watched.FormattingEnabled = true;
            this.listBox_Watched.Location = new System.Drawing.Point(316, 208);
            this.listBox_Watched.Name = "listBox_Watched";
            this.listBox_Watched.Size = new System.Drawing.Size(195, 212);
            this.listBox_Watched.TabIndex = 6;
            this.listBox_Watched.SelectedValueChanged += new System.EventHandler(this.ListBox_Watched_SelectedValueChanged);
            this.listBox_Watched.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ListBox_Watched_MouseDown);
            // 
            // dropdown_Type
            // 
            this.dropdown_Type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dropdown_Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dropdown_Type.FormattingEnabled = true;
            this.dropdown_Type.Location = new System.Drawing.Point(305, 100);
            this.dropdown_Type.Name = "dropdown_Type";
            this.dropdown_Type.Size = new System.Drawing.Size(121, 24);
            this.dropdown_Type.TabIndex = 7;
            // 
            // label_Title
            // 
            this.label_Title.AutoSize = true;
            this.label_Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_Title.Location = new System.Drawing.Point(31, 81);
            this.label_Title.Name = "label_Title";
            this.label_Title.Size = new System.Drawing.Size(30, 15);
            this.label_Title.TabIndex = 8;
            this.label_Title.Text = "Title";
            // 
            // label_Type
            // 
            this.label_Type.AutoSize = true;
            this.label_Type.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_Type.Location = new System.Drawing.Point(302, 79);
            this.label_Type.Name = "label_Type";
            this.label_Type.Size = new System.Drawing.Size(33, 15);
            this.label_Type.TabIndex = 9;
            this.label_Type.Text = "Type";
            // 
            // button_Add
            // 
            this.button_Add.Location = new System.Drawing.Point(432, 100);
            this.button_Add.Name = "button_Add";
            this.button_Add.Size = new System.Drawing.Size(75, 23);
            this.button_Add.TabIndex = 10;
            this.button_Add.Text = "Add";
            this.button_Add.UseVisualStyleBackColor = true;
            this.button_Add.Click += new System.EventHandler(this.Button_Add_Click);
            // 
            // button_MoveLeft
            // 
            this.button_MoveLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_MoveLeft.Location = new System.Drawing.Point(235, 314);
            this.button_MoveLeft.Name = "button_MoveLeft";
            this.button_MoveLeft.Size = new System.Drawing.Size(75, 29);
            this.button_MoveLeft.TabIndex = 11;
            this.button_MoveLeft.Text = "←";
            this.button_MoveLeft.UseVisualStyleBackColor = true;
            this.button_MoveLeft.Click += new System.EventHandler(this.Button_MoveLeft_Click);
            // 
            // button_MoveRight
            // 
            this.button_MoveRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_MoveRight.Location = new System.Drawing.Point(235, 279);
            this.button_MoveRight.Name = "button_MoveRight";
            this.button_MoveRight.Size = new System.Drawing.Size(75, 29);
            this.button_MoveRight.TabIndex = 12;
            this.button_MoveRight.Text = "→";
            this.button_MoveRight.UseVisualStyleBackColor = true;
            this.button_MoveRight.Click += new System.EventHandler(this.Button_MoveRight_Click);
            // 
            // label_VideoTitle
            // 
            this.label_VideoTitle.AutoSize = true;
            this.label_VideoTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_VideoTitle.Location = new System.Drawing.Point(544, 177);
            this.label_VideoTitle.Name = "label_VideoTitle";
            this.label_VideoTitle.Size = new System.Drawing.Size(91, 24);
            this.label_VideoTitle.TabIndex = 13;
            this.label_VideoTitle.Text = "videoTitle";
            this.label_VideoTitle.Visible = false;
            // 
            // label_VideoGenre
            // 
            this.label_VideoGenre.AutoSize = true;
            this.label_VideoGenre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_VideoGenre.Location = new System.Drawing.Point(644, 209);
            this.label_VideoGenre.Name = "label_VideoGenre";
            this.label_VideoGenre.Size = new System.Drawing.Size(43, 16);
            this.label_VideoGenre.TabIndex = 14;
            this.label_VideoGenre.Text = "genre";
            this.label_VideoGenre.Visible = false;
            // 
            // label_VideoDuration
            // 
            this.label_VideoDuration.AutoSize = true;
            this.label_VideoDuration.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_VideoDuration.Location = new System.Drawing.Point(733, 209);
            this.label_VideoDuration.Name = "label_VideoDuration";
            this.label_VideoDuration.Size = new System.Drawing.Size(53, 16);
            this.label_VideoDuration.TabIndex = 15;
            this.label_VideoDuration.Text = "120 min";
            this.label_VideoDuration.Visible = false;
            // 
            // label_VideoRating
            // 
            this.label_VideoRating.AutoSize = true;
            this.label_VideoRating.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_VideoRating.Location = new System.Drawing.Point(761, 177);
            this.label_VideoRating.Name = "label_VideoRating";
            this.label_VideoRating.Size = new System.Drawing.Size(25, 16);
            this.label_VideoRating.TabIndex = 16;
            this.label_VideoRating.Text = "8,4";
            this.label_VideoRating.Visible = false;
            // 
            // textBox_VideoDescription
            // 
            this.textBox_VideoDescription.Enabled = false;
            this.textBox_VideoDescription.Location = new System.Drawing.Point(548, 232);
            this.textBox_VideoDescription.Name = "textBox_VideoDescription";
            this.textBox_VideoDescription.Size = new System.Drawing.Size(256, 124);
            this.textBox_VideoDescription.TabIndex = 17;
            this.textBox_VideoDescription.Text = "This is sample description...";
            this.textBox_VideoDescription.Visible = false;
            // 
            // button_Remove
            // 
            this.button_Remove.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_Remove.Location = new System.Drawing.Point(235, 349);
            this.button_Remove.Name = "button_Remove";
            this.button_Remove.Size = new System.Drawing.Size(75, 29);
            this.button_Remove.TabIndex = 19;
            this.button_Remove.Text = "☓";
            this.button_Remove.UseVisualStyleBackColor = true;
            this.button_Remove.Click += new System.EventHandler(this.Button_Remove_Click);
            // 
            // label_AlreadySaved
            // 
            this.label_AlreadySaved.AutoSize = true;
            this.label_AlreadySaved.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_AlreadySaved.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label_AlreadySaved.Location = new System.Drawing.Point(31, 132);
            this.label_AlreadySaved.Name = "label_AlreadySaved";
            this.label_AlreadySaved.Size = new System.Drawing.Size(142, 15);
            this.label_AlreadySaved.TabIndex = 20;
            this.label_AlreadySaved.Text = "This title is already saved";
            this.label_AlreadySaved.Visible = false;
            // 
            // label_VideoYear
            // 
            this.label_VideoYear.AutoSize = true;
            this.label_VideoYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_VideoYear.Location = new System.Drawing.Point(566, 209);
            this.label_VideoYear.Name = "label_VideoYear";
            this.label_VideoYear.Size = new System.Drawing.Size(36, 16);
            this.label_VideoYear.TabIndex = 21;
            this.label_VideoYear.Text = "2018";
            this.label_VideoYear.Visible = false;
            // 
            // button_EditVideo
            // 
            this.button_EditVideo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button_EditVideo.Location = new System.Drawing.Point(628, 387);
            this.button_EditVideo.Name = "button_EditVideo";
            this.button_EditVideo.Size = new System.Drawing.Size(97, 23);
            this.button_EditVideo.TabIndex = 22;
            this.button_EditVideo.Text = "Edit";
            this.button_EditVideo.UseVisualStyleBackColor = true;
            this.button_EditVideo.Visible = false;
            this.button_EditVideo.Click += new System.EventHandler(this.Button_EditVideo_Click);
            // 
            // linkLabel_WWW
            // 
            this.linkLabel_WWW.AutoSize = true;
            this.linkLabel_WWW.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.linkLabel_WWW.Location = new System.Drawing.Point(655, 365);
            this.linkLabel_WWW.Name = "linkLabel_WWW";
            this.linkLabel_WWW.Size = new System.Drawing.Size(40, 13);
            this.linkLabel_WWW.TabIndex = 23;
            this.linkLabel_WWW.TabStop = true;
            this.linkLabel_WWW.Text = "WWW";
            this.linkLabel_WWW.Visible = false;
            this.linkLabel_WWW.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabel_WWW_LinkClicked);
            // 
            // label_AutosaveInfo
            // 
            this.label_AutosaveInfo.AutoSize = true;
            this.label_AutosaveInfo.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label_AutosaveInfo.Location = new System.Drawing.Point(655, 9);
            this.label_AutosaveInfo.Name = "label_AutosaveInfo";
            this.label_AutosaveInfo.Size = new System.Drawing.Size(167, 26);
            this.label_AutosaveInfo.TabIndex = 24;
            this.label_AutosaveInfo.Text = "Videos will be saved automatically\r\nafter exiting the program";
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 445);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(834, 22);
            this.statusStrip.SizingGrip = false;
            this.statusStrip.TabIndex = 25;
            this.statusStrip.Text = "statusStrip";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(130, 17);
            this.toolStripStatusLabel.Text = "© 2018 Jakub Gawenda";
            // 
            // comboBox_SortBy
            // 
            this.comboBox_SortBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_SortBy.FormattingEnabled = true;
            this.comboBox_SortBy.Location = new System.Drawing.Point(235, 230);
            this.comboBox_SortBy.Name = "comboBox_SortBy";
            this.comboBox_SortBy.Size = new System.Drawing.Size(75, 21);
            this.comboBox_SortBy.TabIndex = 26;
            this.comboBox_SortBy.SelectedIndexChanged += new System.EventHandler(this.ComboBox_SortBy_SelectedIndexChanged);
            // 
            // label_SortBy
            // 
            this.label_SortBy.AutoSize = true;
            this.label_SortBy.Location = new System.Drawing.Point(236, 211);
            this.label_SortBy.Name = "label_SortBy";
            this.label_SortBy.Size = new System.Drawing.Size(40, 13);
            this.label_SortBy.TabIndex = 27;
            this.label_SortBy.Text = "Sort by";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 467);
            this.Controls.Add(this.label_SortBy);
            this.Controls.Add(this.comboBox_SortBy);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.label_AutosaveInfo);
            this.Controls.Add(this.linkLabel_WWW);
            this.Controls.Add(this.button_EditVideo);
            this.Controls.Add(this.label_VideoYear);
            this.Controls.Add(this.label_AlreadySaved);
            this.Controls.Add(this.button_Remove);
            this.Controls.Add(this.textBox_VideoDescription);
            this.Controls.Add(this.label_VideoRating);
            this.Controls.Add(this.label_VideoDuration);
            this.Controls.Add(this.label_VideoGenre);
            this.Controls.Add(this.label_VideoTitle);
            this.Controls.Add(this.button_MoveRight);
            this.Controls.Add(this.button_MoveLeft);
            this.Controls.Add(this.button_Add);
            this.Controls.Add(this.label_Type);
            this.Controls.Add(this.label_Title);
            this.Controls.Add(this.dropdown_Type);
            this.Controls.Add(this.listBox_Watched);
            this.Controls.Add(this.label_Watched);
            this.Controls.Add(this.label_ToWatch);
            this.Controls.Add(this.label_AddNew);
            this.Controls.Add(this.textBox_AddNew);
            this.Controls.Add(this.listBox_ToWatch);
            this.Controls.Add(this.label_Header);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Video Library";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.EnabledChanged += new System.EventHandler(this.MainForm_EnabledChanged);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_Header;
        private System.Windows.Forms.ListBox listBox_ToWatch;
        private System.Windows.Forms.TextBox textBox_AddNew;
        private System.Windows.Forms.Label label_AddNew;
        private System.Windows.Forms.Label label_ToWatch;
        private System.Windows.Forms.Label label_Watched;
        private System.Windows.Forms.ListBox listBox_Watched;
        private System.Windows.Forms.ComboBox dropdown_Type;
        private System.Windows.Forms.Label label_Title;
        private System.Windows.Forms.Label label_Type;
        private System.Windows.Forms.Button button_Add;
        private System.Windows.Forms.Button button_MoveLeft;
        private System.Windows.Forms.Button button_MoveRight;
        private System.Windows.Forms.Label label_VideoTitle;
        private System.Windows.Forms.Label label_VideoGenre;
        private System.Windows.Forms.Label label_VideoDuration;
        private System.Windows.Forms.Label label_VideoRating;
        private System.Windows.Forms.RichTextBox textBox_VideoDescription;
        private System.Windows.Forms.Button button_Remove;
        private System.Windows.Forms.Label label_AlreadySaved;
        private System.Windows.Forms.Label label_VideoYear;
        private System.Windows.Forms.Button button_EditVideo;
        private System.Windows.Forms.LinkLabel linkLabel_WWW;
        private System.Windows.Forms.Label label_AutosaveInfo;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ComboBox comboBox_SortBy;
        private System.Windows.Forms.Label label_SortBy;
    }
}

