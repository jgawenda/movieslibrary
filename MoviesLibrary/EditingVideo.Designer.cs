﻿namespace MoviesLibrary
{
    partial class EditingVideo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditingVideo));
            this.label_Title = new System.Windows.Forms.Label();
            this.textBox_Title = new System.Windows.Forms.TextBox();
            this.textBox_Rating = new System.Windows.Forms.TextBox();
            this.label_Rating = new System.Windows.Forms.Label();
            this.textBox_Year = new System.Windows.Forms.TextBox();
            this.label_Year = new System.Windows.Forms.Label();
            this.label_Genre = new System.Windows.Forms.Label();
            this.comboBox_Genre = new System.Windows.Forms.ComboBox();
            this.checkBox_Favorite = new System.Windows.Forms.CheckBox();
            this.textBox_URL = new System.Windows.Forms.TextBox();
            this.label_URL = new System.Windows.Forms.Label();
            this.label_Description = new System.Windows.Forms.Label();
            this.textBox_Description = new System.Windows.Forms.RichTextBox();
            this.button_Save = new System.Windows.Forms.Button();
            this.textBox_Length = new System.Windows.Forms.TextBox();
            this.label_Length = new System.Windows.Forms.Label();
            this.label_Minutes = new System.Windows.Forms.Label();
            this.label_Episodes = new System.Windows.Forms.Label();
            this.label_Season = new System.Windows.Forms.Label();
            this.textBox_Episodes = new System.Windows.Forms.TextBox();
            this.button_AddSeason = new System.Windows.Forms.Button();
            this.button_RemoveSeason = new System.Windows.Forms.Button();
            this.comboBox_Seasons = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label_Title
            // 
            this.label_Title.AutoSize = true;
            this.label_Title.Location = new System.Drawing.Point(12, 19);
            this.label_Title.Name = "label_Title";
            this.label_Title.Size = new System.Drawing.Size(27, 13);
            this.label_Title.TabIndex = 0;
            this.label_Title.Text = "Title";
            // 
            // textBox_Title
            // 
            this.textBox_Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_Title.Location = new System.Drawing.Point(45, 14);
            this.textBox_Title.Name = "textBox_Title";
            this.textBox_Title.Size = new System.Drawing.Size(298, 22);
            this.textBox_Title.TabIndex = 2;
            // 
            // textBox_Rating
            // 
            this.textBox_Rating.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_Rating.Location = new System.Drawing.Point(153, 42);
            this.textBox_Rating.MaxLength = 3;
            this.textBox_Rating.Name = "textBox_Rating";
            this.textBox_Rating.Size = new System.Drawing.Size(61, 22);
            this.textBox_Rating.TabIndex = 4;
            // 
            // label_Rating
            // 
            this.label_Rating.AutoSize = true;
            this.label_Rating.Location = new System.Drawing.Point(111, 47);
            this.label_Rating.Name = "label_Rating";
            this.label_Rating.Size = new System.Drawing.Size(38, 13);
            this.label_Rating.TabIndex = 3;
            this.label_Rating.Text = "Rating";
            // 
            // textBox_Year
            // 
            this.textBox_Year.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_Year.Location = new System.Drawing.Point(282, 42);
            this.textBox_Year.MaxLength = 4;
            this.textBox_Year.Name = "textBox_Year";
            this.textBox_Year.Size = new System.Drawing.Size(61, 22);
            this.textBox_Year.TabIndex = 6;
            // 
            // label_Year
            // 
            this.label_Year.AutoSize = true;
            this.label_Year.Location = new System.Drawing.Point(249, 47);
            this.label_Year.Name = "label_Year";
            this.label_Year.Size = new System.Drawing.Size(29, 13);
            this.label_Year.TabIndex = 5;
            this.label_Year.Text = "Year";
            // 
            // label_Genre
            // 
            this.label_Genre.AutoSize = true;
            this.label_Genre.Location = new System.Drawing.Point(86, 86);
            this.label_Genre.Name = "label_Genre";
            this.label_Genre.Size = new System.Drawing.Size(36, 13);
            this.label_Genre.TabIndex = 7;
            this.label_Genre.Text = "Genre";
            // 
            // comboBox_Genre
            // 
            this.comboBox_Genre.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Genre.FormattingEnabled = true;
            this.comboBox_Genre.Location = new System.Drawing.Point(128, 83);
            this.comboBox_Genre.Name = "comboBox_Genre";
            this.comboBox_Genre.Size = new System.Drawing.Size(121, 21);
            this.comboBox_Genre.TabIndex = 8;
            // 
            // checkBox_Favorite
            // 
            this.checkBox_Favorite.AutoSize = true;
            this.checkBox_Favorite.Location = new System.Drawing.Point(17, 47);
            this.checkBox_Favorite.Name = "checkBox_Favorite";
            this.checkBox_Favorite.Size = new System.Drawing.Size(64, 17);
            this.checkBox_Favorite.TabIndex = 9;
            this.checkBox_Favorite.Text = "Favorite";
            this.checkBox_Favorite.UseVisualStyleBackColor = true;
            // 
            // textBox_URL
            // 
            this.textBox_URL.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_URL.Location = new System.Drawing.Point(58, 189);
            this.textBox_URL.Name = "textBox_URL";
            this.textBox_URL.Size = new System.Drawing.Size(285, 22);
            this.textBox_URL.TabIndex = 11;
            // 
            // label_URL
            // 
            this.label_URL.AutoSize = true;
            this.label_URL.Location = new System.Drawing.Point(12, 194);
            this.label_URL.Name = "label_URL";
            this.label_URL.Size = new System.Drawing.Size(40, 13);
            this.label_URL.TabIndex = 10;
            this.label_URL.Text = "WWW";
            // 
            // label_Description
            // 
            this.label_Description.AutoSize = true;
            this.label_Description.Location = new System.Drawing.Point(12, 222);
            this.label_Description.Name = "label_Description";
            this.label_Description.Size = new System.Drawing.Size(60, 13);
            this.label_Description.TabIndex = 12;
            this.label_Description.Text = "Description";
            // 
            // textBox_Description
            // 
            this.textBox_Description.Location = new System.Drawing.Point(12, 238);
            this.textBox_Description.Name = "textBox_Description";
            this.textBox_Description.Size = new System.Drawing.Size(331, 127);
            this.textBox_Description.TabIndex = 13;
            this.textBox_Description.Text = "";
            // 
            // button_Save
            // 
            this.button_Save.Location = new System.Drawing.Point(141, 380);
            this.button_Save.Name = "button_Save";
            this.button_Save.Size = new System.Drawing.Size(75, 23);
            this.button_Save.TabIndex = 14;
            this.button_Save.Text = "Save";
            this.button_Save.UseVisualStyleBackColor = true;
            this.button_Save.Click += new System.EventHandler(this.Button_Save_Click);
            // 
            // textBox_Length
            // 
            this.textBox_Length.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_Length.Location = new System.Drawing.Point(244, 137);
            this.textBox_Length.MaxLength = 4;
            this.textBox_Length.Name = "textBox_Length";
            this.textBox_Length.Size = new System.Drawing.Size(61, 22);
            this.textBox_Length.TabIndex = 16;
            this.textBox_Length.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label_Length
            // 
            this.label_Length.AutoSize = true;
            this.label_Length.Location = new System.Drawing.Point(200, 142);
            this.label_Length.Name = "label_Length";
            this.label_Length.Size = new System.Drawing.Size(40, 13);
            this.label_Length.TabIndex = 15;
            this.label_Length.Text = "Length";
            // 
            // label_Minutes
            // 
            this.label_Minutes.AutoSize = true;
            this.label_Minutes.Location = new System.Drawing.Point(311, 142);
            this.label_Minutes.Name = "label_Minutes";
            this.label_Minutes.Size = new System.Drawing.Size(23, 13);
            this.label_Minutes.TabIndex = 17;
            this.label_Minutes.Text = "min";
            // 
            // label_Episodes
            // 
            this.label_Episodes.AutoSize = true;
            this.label_Episodes.Location = new System.Drawing.Point(86, 156);
            this.label_Episodes.Name = "label_Episodes";
            this.label_Episodes.Size = new System.Drawing.Size(50, 13);
            this.label_Episodes.TabIndex = 20;
            this.label_Episodes.Text = "Episodes";
            // 
            // label_Season
            // 
            this.label_Season.AutoSize = true;
            this.label_Season.Location = new System.Drawing.Point(88, 128);
            this.label_Season.Name = "label_Season";
            this.label_Season.Size = new System.Drawing.Size(43, 13);
            this.label_Season.TabIndex = 18;
            this.label_Season.Text = "Season";
            // 
            // textBox_Episodes
            // 
            this.textBox_Episodes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_Episodes.Location = new System.Drawing.Point(142, 151);
            this.textBox_Episodes.MaxLength = 3;
            this.textBox_Episodes.Name = "textBox_Episodes";
            this.textBox_Episodes.Size = new System.Drawing.Size(36, 22);
            this.textBox_Episodes.TabIndex = 21;
            this.textBox_Episodes.TextChanged += new System.EventHandler(this.TextBox_Episodes_TextChanged);
            // 
            // button_AddSeason
            // 
            this.button_AddSeason.Location = new System.Drawing.Point(15, 123);
            this.button_AddSeason.Name = "button_AddSeason";
            this.button_AddSeason.Size = new System.Drawing.Size(26, 23);
            this.button_AddSeason.TabIndex = 22;
            this.button_AddSeason.Text = "+";
            this.button_AddSeason.UseVisualStyleBackColor = true;
            this.button_AddSeason.Click += new System.EventHandler(this.Button_AddSeason_Click);
            // 
            // button_RemoveSeason
            // 
            this.button_RemoveSeason.Location = new System.Drawing.Point(45, 123);
            this.button_RemoveSeason.Name = "button_RemoveSeason";
            this.button_RemoveSeason.Size = new System.Drawing.Size(26, 23);
            this.button_RemoveSeason.TabIndex = 23;
            this.button_RemoveSeason.Text = "-";
            this.button_RemoveSeason.UseVisualStyleBackColor = true;
            this.button_RemoveSeason.Click += new System.EventHandler(this.Button_RemoveSeason_Click);
            // 
            // comboBox_Seasons
            // 
            this.comboBox_Seasons.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Seasons.FormattingEnabled = true;
            this.comboBox_Seasons.Location = new System.Drawing.Point(142, 125);
            this.comboBox_Seasons.Name = "comboBox_Seasons";
            this.comboBox_Seasons.Size = new System.Drawing.Size(36, 21);
            this.comboBox_Seasons.TabIndex = 24;
            this.comboBox_Seasons.SelectedIndexChanged += new System.EventHandler(this.ComboBox_Seasons_SelectedIndexChanged);
            // 
            // EditingVideo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(355, 417);
            this.Controls.Add(this.comboBox_Seasons);
            this.Controls.Add(this.button_RemoveSeason);
            this.Controls.Add(this.button_AddSeason);
            this.Controls.Add(this.textBox_Episodes);
            this.Controls.Add(this.label_Episodes);
            this.Controls.Add(this.label_Season);
            this.Controls.Add(this.label_Minutes);
            this.Controls.Add(this.textBox_Length);
            this.Controls.Add(this.label_Length);
            this.Controls.Add(this.button_Save);
            this.Controls.Add(this.textBox_Description);
            this.Controls.Add(this.label_Description);
            this.Controls.Add(this.textBox_URL);
            this.Controls.Add(this.label_URL);
            this.Controls.Add(this.checkBox_Favorite);
            this.Controls.Add(this.comboBox_Genre);
            this.Controls.Add(this.label_Genre);
            this.Controls.Add(this.textBox_Year);
            this.Controls.Add(this.label_Year);
            this.Controls.Add(this.textBox_Rating);
            this.Controls.Add(this.label_Rating);
            this.Controls.Add(this.textBox_Title);
            this.Controls.Add(this.label_Title);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EditingVideo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Editing Video";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.EditingVideo_FormClosed);
            this.Load += new System.EventHandler(this.EditingVideo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_Title;
        private System.Windows.Forms.TextBox textBox_Title;
        private System.Windows.Forms.TextBox textBox_Rating;
        private System.Windows.Forms.Label label_Rating;
        private System.Windows.Forms.TextBox textBox_Year;
        private System.Windows.Forms.Label label_Year;
        private System.Windows.Forms.Label label_Genre;
        private System.Windows.Forms.ComboBox comboBox_Genre;
        private System.Windows.Forms.CheckBox checkBox_Favorite;
        private System.Windows.Forms.TextBox textBox_URL;
        private System.Windows.Forms.Label label_URL;
        private System.Windows.Forms.Label label_Description;
        private System.Windows.Forms.RichTextBox textBox_Description;
        private System.Windows.Forms.Button button_Save;
        private System.Windows.Forms.TextBox textBox_Length;
        private System.Windows.Forms.Label label_Length;
        private System.Windows.Forms.Label label_Minutes;
        private System.Windows.Forms.Label label_Episodes;
        private System.Windows.Forms.Label label_Season;
        private System.Windows.Forms.TextBox textBox_Episodes;
        private System.Windows.Forms.Button button_AddSeason;
        private System.Windows.Forms.Button button_RemoveSeason;
        private System.Windows.Forms.ComboBox comboBox_Seasons;
    }
}